datalife_purchase_supplier_price_standalone
===========================================

The purchase_supplier_price_standalone module of the Tryton application platform.

[![Build Status](https://drone.io/gitlab.com/datalifeit/trytond-purchase_supplier_price_standalone/status.png)](https://drone.io/gitlab.com/datalifeit/trytond-purchase_supplier_price_standalone/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
