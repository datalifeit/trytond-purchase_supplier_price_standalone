# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_purchase_supplier_price_standalone import suite

__all__ = ['suite']
